import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import axios from 'axios';

function App() {
  const baseUrl="http://localhost:8888/smartsoft/backend/";
  const urlProduct="http://localhost:8888/smartsoft/backend/product.php";
  const [data, setData]=useState([]);
  const [total, setTotal]=useState([]);
  const [products, setProducts]=useState([]);
  const [modalInsertar, setModalInsertar]= useState(false);
  const [modalEditar, setModalEditar]= useState(false);
  const [modalShow, setModalShow]= useState(false);
  const [modalEliminar, setModalEliminar]= useState(false);
  const [frameworkSeleccionado, setFrameworkSeleccionado]=useState({
    id: '',
    code: '',
    total: '',
    items: []
  });

  const handleChange=e=>{
    let {name, value}=e.target;
    let selectedOption=(e.target.selectedOptions);
    const selected=[];
    let total = 0;

    if(e.target.name == 'items'){
      for (let i = 0; i < selectedOption.length; i++){
        selected.push(selectedOption.item(i).value);
      }
      value = selected;
      products.forEach(element => {
        if(selected.includes(element.id)){
          total = (element.value)*1 + (total)*1;        
        }
      });
      setTotal(total);

    }
    console.log(total);
    setFrameworkSeleccionado((prevState)=>({
      ...prevState,
      [name]: value
    }))
    console.log(frameworkSeleccionado);
  }

  const abrirCerrarModalInsertar=()=>{
    setModalInsertar(!modalInsertar);
  }

  const abrirCerrarModalShow=()=>{
    setModalShow(!modalShow);
  }

  const abrirCerrarModalEditar=()=>{
    setModalEditar(!modalEditar);
  }

  const abrirCerrarModalEliminar=()=>{
    setModalEliminar(!modalEliminar);
  }

  const peticionGet=async()=>{
    await axios.get(baseUrl)
    .then(response=>{
      setData(response.data);
    }).catch(error=>{
      console.log(error);
    })
  }

  const productGet=async()=>{
    await axios.get(urlProduct)
    .then(response=>{
      setProducts(response.data);
    }).catch(error=>{
      console.log(error);
    })
  }

  const peticionPost=async()=>{
    var f = new FormData();
    f.append("code", frameworkSeleccionado.code);
    f.append("total", frameworkSeleccionado.total);
    f.append("items", frameworkSeleccionado.items);
    f.append("METHOD", "POST");
    await axios.post(baseUrl, f)
    .then(response=>{
      setData(data.concat(response.data));
      abrirCerrarModalInsertar();
    }).catch(error=>{
      console.log(error);
    })
  }


  const peticionPut=async()=>{
    var f = new FormData();
    f.append("code", frameworkSeleccionado.code);
    f.append("total", frameworkSeleccionado.total);
    f.append("created_at", frameworkSeleccionado.created_at);
    f.append("METHOD", "PUT");
    await axios.post(baseUrl, f, {params: {id: frameworkSeleccionado.id}})
    .then(response=>{
      var dataNueva= data;
      dataNueva.map(framework=>{
        if(framework.id===frameworkSeleccionado.id){
          framework.code=frameworkSeleccionado.code;
          framework.total=frameworkSeleccionado.total;
          framework.created_at=frameworkSeleccionado.created_at;
        }
      });
      setData(dataNueva);
      abrirCerrarModalEditar();
    }).catch(error=>{
      console.log(error);
    })
  }

  const peticionDelete=async()=>{
    var f = new FormData();
    f.append("METHOD", "DELETE");
    await axios.post(baseUrl, f, {params: {id: frameworkSeleccionado.id}})
    .then(response=>{
      setData(data.filter(framework=>framework.id!==frameworkSeleccionado.id));
      abrirCerrarModalEliminar();
    }).catch(error=>{
      console.log(error);
    })
  }

  const seleccionarFramework=(framework, caso)=>{
    setFrameworkSeleccionado(framework);

    (caso==="Show")?
    abrirCerrarModalShow():
    abrirCerrarModalEliminar()
  }

  useEffect(()=>{
    peticionGet();
    productGet();
  },[])

  return (
    <div style={{textAlign: 'center'}}>
<br />
      <button className="btn btn-success" onClick={()=>abrirCerrarModalInsertar()}>Insertar</button>
      <br /><br />
    <table className="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Code</th>
          <th>Total</th>
          <th>Date</th>
          <th>Accions</th>
        </tr>
      </thead>
      <tbody>
        
        {data.map(invoice=>(
          <tr key={invoice.id}>
            <td>{invoice.id}</td>
            <td>{invoice.code}</td>
            <td>{invoice.total}</td>
            <td>{invoice.created_at}</td>
          <td>
          <button className="btn btn-primary" onClick={()=>seleccionarFramework(invoice, "Show")}>Ver</button> {"  "}
          {/* <button className="btn btn-danger" onClick={()=>seleccionarFramework(invoice, "Eliminar")}>Eliminar</button> */}
          </td>
          </tr>
        ))}


      </tbody> 

    </table>


    <Modal isOpen={modalInsertar}>
      <ModalHeader>Generar Factura</ModalHeader>
      <ModalBody>
        <div className="form-group">
          <label>Code: </label>
          <br />
          <input type="text" className="form-control" name="code" onChange={handleChange}/>
          <br />
          <label>Total: </label>
          <br />
          <input type="text" className="form-control" name="total" value={total} readOnly="true"/>
          <br />
          <br />
          <label>Seleccionar Items: (ctr/cmd) </label>
          <select className="form-control" name="items" multiple onChange={handleChange}>
            {
              products.map(item => (
                <option value={item.id}>{item.name}  ${item.value}</option>
              ))
            }
            </select>
        </div>
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-primary" onClick={()=>peticionPost()}>Insertar</button>{"   "}
        <button className="btn btn-danger" onClick={()=>abrirCerrarModalInsertar()}>Cancelar</button>
      </ModalFooter>
    </Modal>

    <Modal isOpen={modalShow}>
      <ModalHeader>Factura</ModalHeader>
      <ModalBody>
        <div className="form-group">
          <label>Code: </label>
          <br />
          <input type="text" className="form-control" name="code" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.code}/>
          <br />
          <label>Total: </label>
          <br />
          <input type="text" className="form-control" name="total" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.total}/>
          <br />
          <label>Date: </label>
          <br />
          <input type="text" className="form-control" name="created_at" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.created_at}/>
          <br />
        </div>
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-danger" onClick={()=>abrirCerrarModalShow()}>Cerrar</button>
      </ModalFooter>
    </Modal>
    
    <Modal isOpen={modalEditar}>
      <ModalHeader>Editar Factura</ModalHeader>
      <ModalBody>
        <div className="form-group">
          <label>Code: </label>
          <br />
          <input type="text" className="form-control" name="code" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.code}/>
          <br />
          <label>Total: </label>
          <br />
          <input type="text" className="form-control" name="total" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.total}/>
          <br />
          <label>Date: </label>
          <br />
          <input type="text" className="form-control" name="created_at" onChange={handleChange} value={frameworkSeleccionado && frameworkSeleccionado.created_at}/>
          <br />
        </div>
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-primary" onClick={()=>peticionPut()}>Editar</button>{"   "}
        <button className="btn btn-danger" onClick={()=>abrirCerrarModalEditar()}>Cancelar</button>
      </ModalFooter>
    </Modal>

    <Modal isOpen={modalEliminar}>
        <ModalBody>
        ¿Estás seguro que deseas eliminar la Factura {frameworkSeleccionado && frameworkSeleccionado.code}?
        </ModalBody>
        <ModalFooter>
          <button className="btn btn-danger" onClick={()=>peticionDelete()}>
            Sí
          </button>
          <button
            className="btn btn-secondary"
            onClick={()=>abrirCerrarModalEliminar()}
          >
            No
          </button>
        </ModalFooter>
      </Modal>

    </div>
  );
}

export default App;
