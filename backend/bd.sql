CREATE TABLE product(
  id          int(11) NOT NULL auto_increment PRIMARY KEY,
  name        varchar(77) NOT NULL,
  value       int(11) NOT NULL,
  image       varchar(200) NOT NULL,
  created_at datetime DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT CURRENT_TIMESTAMP,
  user_updated int(11)
)ENGINE=InnoDB;

CREATE TABLE invoiceheader(
  id          int(11) NOT NULL auto_increment PRIMARY KEY,
  code        varchar(20) NOT NULL,
  total       int(11) NOT NULL,
  created_at datetime DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT CURRENT_TIMESTAMP,
  user_updated int(11)
)ENGINE=InnoDB;

CREATE TABLE invoicebody(
  id                int(11) NOT NULL auto_increment PRIMARY KEY,
  id_invoiceheader  integer NOT NULL,
  id_product        integer NOT NULL,
  qty               integer DEFAULT 1 NOT NULL,
  value       int(11) NOT NULL,
  total       int(11) NOT NULL,
  created_at datetime DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT CURRENT_TIMESTAMP,
  user_updated int(11)
)ENGINE=InnoDB;


INSERT INTO product(id, name, value, image) VALUES
(1, 'Hamburguesa Vegetariana', 24000, 'https://pastoshop.com/archivos/product-1-1590634481.jpg'),
(2, 'Pijama', 133000, 'https://pastoshop.com/archivos/product-1-1590635036.png'),
(3, 'Dominios', 80000, 'https://pastoshop.com/archivos/product-1-1590634961.jpg');