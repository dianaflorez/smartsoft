<?php

include 'bd/BD.php';

header('Access-Control-Allow-Origin: *');

if($_SERVER['REQUEST_METHOD']=='GET'){
    if(isset($_GET['id'])){
        $query="select * from invoiceheader where id=".$_GET['id'];
        $resultado=metodoGet($query);
        echo json_encode($resultado->fetch(PDO::FETCH_ASSOC));
    }else{
        $query="select * from invoiceheader";
        $resultado=metodoGet($query);
        echo json_encode($resultado->fetchAll()); 
    }
    header("HTTP/1.1 200 OK");
    exit();
}

// if($_SERVER['REQUEST_METHOD']=='POST'){
// //if($_POST['METHOD']=='POST'){
//     unset($_POST['METHOD']);
//     $code=$_POST['code'];
//     $total=$_POST['total'];
//     $created_at=$_POST['created_at'];
//     $query="insert into invoiceheader(code, total, created_at) values ('$code', '$total', '$created_at')";
//     $queryAutoIncrement="select MAX(id) as id from invoiceheader";
//     $resultado=metodoPost($query, $queryAutoIncrement);
//     echo json_encode($resultado);
//     header("HTTP/1.1 200 OK");
//     exit();
// }

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $input = $_POST;
    $code = $_POST['code'];
    $total = $_POST['total'];
    $item = $_POST['items'];
    $sql = "INSERT INTO invoiceheader
          (code, total)
          VALUES
          ($code, $total)";
    $queryAutoIncrement="select MAX(id) as id from invoiceheader";
    $resultado=metodoPost($sql, $queryAutoIncrement);
    $idInvoiceheader = $resultado['id'];
    // insert items
    foreach($item as $i){
        $sqlProduct="select value from product where id =$i";
        $vlr = metodoGetone($sqlProduct);
        $resvlr = $vlr->fetch(PDO::FETCH_ASSOC);

        $productValue = $resvlr['value'];
        $sql = "INSERT INTO invoicebody
            (id_invoiceheader, id_product, qty, value, total)
            VALUES
            ($idInvoiceheader, $i, 1, $productValue, $productValue )";
        $queryAutoIncrementItem = "select MAX(id) as id from invoicebody";
        $resultado = metodoPost($sql, $queryAutoIncrementItem);
    }

    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}


if($_POST['METHOD']=='PUT'){
    unset($_POST['METHOD']);
    $id=$_GET['id'];
    $code=$_POST['code'];
    $total=$_POST['total'];
    $created_at=$_POST['created_at'];
    $query="UPDATE invoiceheader SET code='$code', total='$total', created_at='$created_at' WHERE id='$id'";
    $resultado=metodoPut($query);
    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}

if($_POST['METHOD']=='DELETE'){
    unset($_POST['METHOD']);
    $id=$_GET['id'];
    $query="DELETE FROM invoiceheader WHERE id='$id'";
    $resultado=metodoDelete($query);
    echo json_encode($resultado);
    header("HTTP/1.1 200 OK");
    exit();
}

header("HTTP/1.1 400 Bad Request");


?>